class Spaceship extends Phaser.Sprite {

    constructor(game, scene, x = 300, y = 300, enemyAttackSound) {

        const PLAYER_DRAG = 130;
        const PLAYER_VELOCITY = 200;
        const MAX_HP = 20;
        const ENEMY_TO_WIN = 100;

        super(game, x, y, 'spaceship');

        game.physics.arcade.enable(this);
        game.add.existing(this);

        this.anchor.set(0.5);
        this.scene = scene;
        this.body.drag.set(PLAYER_DRAG);
        this.body.maxVelocity.set(PLAYER_VELOCITY);
        this.body.collideWorldBounds = true;
        this.weaponSet = [];
        this.weaponQty = 0;
        this.weaponIndex = 0;
        this.maxHp = MAX_HP;
        this.hp = this.maxHp;
        this.enemiesKilled = 0;
        this.enemiesToWin = ENEMY_TO_WIN;
        this.hpText = null;
        this.enemiesKilledText = null;
        this.enemyAttackSound = enemyAttackSound;

        let changeKey = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
        changeKey.onDown.add(this.changeWeapon, this.game);
    }

    addWeapon(n, image, bulletSpeed = 600, fireRate = 150) {
        let weapon = this.game.add.weapon(n, image);
        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        weapon.bulletSpeed = bulletSpeed;
        weapon.fireRate = fireRate;
        weapon.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        weapon.trackSprite(this, 0, 0, true);
        this.weaponQty = this.weaponSet.push(weapon);
    }

    fireWeapon() {
        this.weaponSet[this.weaponIndex].fire();
    }

    updatePlayer() {

        //FORWARD
        if (this.scene.cursors.up.isDown) {
            this.game.physics.arcade.accelerationFromRotation(this.rotation, 300, this.body.acceleration);
        } else {
            this.body.acceleration.set(0);
        }

        //ROTATION
        if (this.scene.cursors.left.isDown) {
            this.body.angularVelocity = -300;
        } else if (this.scene.cursors.right.isDown) {
            this.body.angularVelocity = 300;
        } else {
            this.body.angularVelocity = 0;
        }

        //FIRE
        if (this.scene.fireButton.isDown) {
            this.fireWeapon();
        }
    }

    countEnemy() {
        this.enemiesKilled++;
        if (this.enemiesKilled % 10 == 0) {
            this.scene.maxEnemies++;
        }
        this.enemiesKilledText.setText(`Enemies Killed ${this.enemiesKilled} / ${this.enemiesToWin}`);
        if (this.enemiesKilled >= this.enemiesToWin) {
            this.game.state.start('Win');
        }
    }

    changeWeapon() {
        //TODO
    }

    static onEnemyAttack(player, enemy) {
        enemy.kill();
        player.enemyAttackSound.play();
        player.hp--;
        player.hpText.setText(`HP ${player.hp} / ${player.maxHp}`);
        if (player.hp <= 0) {
            player.scene.backgroundMusic.stop();
            player.game.state.start('GameOver');
        }
    }

    setHpText(hpText) {
        this.hpText = hpText;
    }

    setEnemiesKilledText(enemiesKilledText) {
        this.enemiesKilledText = enemiesKilledText;
    }
}

export default Spaceship;