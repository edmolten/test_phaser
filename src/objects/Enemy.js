class Enemy extends Phaser.Sprite {

    constructor(game, player, x = 0, y = 0) {
        super(game, x, y, 'enemy');

        game.physics.arcade.enable(this);
        game.add.existing(this);

        this.anchor.set(0.5);
        // this.body.maxVelocity.set(game.rnd.integerInRange(100, 500));
        this.hp = 2;
        this.player = player;
        this.enemyDestroySound = this.game.add.audio('enemyDestroy');
    }

    updateEnemy() {
        this.game.physics.arcade.moveToObject(this, this.player, 150);
        this.body.rotation += 2;
    }

    static getRandomStartPoint(gameScene, gameSize) {
        let x = gameScene.game.rnd.integerInRange(-30, gameSize + 30);
        let y = 0;
        if (gameScene.enemySide % 2 == 0) {
            y = -30;
            gameScene.enemySide++;
        } else {
            y = gameSize + 30;
            gameScene.enemySide++;
        }
        return {
            x: x,
            y: y
        };
    }

    static onBulletHit(bullet, enemy) {
        enemy.hp -= 1;
        if (enemy.hp <= 0) {
            enemy.enemyDestroySound.play();
            enemy.kill();
            this.player.countEnemy();
        }
        bullet.kill();
    }

    static createEnemy(gameScene) {
        let point = Enemy.getRandomStartPoint(gameScene, gameScene.game.world.width);
        gameScene.enemies.add(new Enemy(gameScene.game, gameScene.player, point.x, point.y));
    }
}

export default Enemy;