import MainMenu from 'states/MainMenu';
import GameScene from 'states/GameScene';
import GameOver from 'states/GameOver';
import Win from 'states/Win';

class Game extends Phaser.Game {

    constructor() {
        const gameX = 1280;
        const gameY = 720;

        super(gameX, gameY, Phaser.AUTO, 'content', null);

        this.state.add('MainMenu', MainMenu, false);
        this.state.add('GameScene', GameScene, false);
        this.state.add('GameOver', GameOver, false);
        this.state.add('Win', Win, false);
        this.state.start('MainMenu');
    }
}

var game = new Game();