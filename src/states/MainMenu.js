class MainMenu extends Phaser.State {

    preload() {
        this.game.load.image('start', 'assets/start.png');
    }

    create() {
        let mainMenuGroup = this.game.add.group();
        let buttonX = this.game.world.centerX;
        let buttonY = this.game.world.centerY;
        let startButton = this.game.make.button(buttonX, buttonY, 'start', this.actionStart, this, 2, 1, 0);
        startButton.anchor.set(0.5);
        mainMenuGroup.add(startButton);
    }

    actionStart() {
        this.game.state.start('GameScene');
    }

}

export default MainMenu;