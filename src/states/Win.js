class Win extends Phaser.State {

	create() {
		const textStyle = {
        	font: "40px Arial",
        	fill: "#00ff00",
       		align: "center"
    	}
		let hpText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "YOU WIN", textStyle);
		hpText.anchor.set(0.5);
	}
   
}

export default Win;