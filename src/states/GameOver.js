class GameOver extends Phaser.State {

    preload() {
        this.game.load.image('restart', 'assets/restart.png');
    }

    create() {
        const textStyle = {
            font: "40px Arial",
            fill: "#ff0000",
            align: "center"
        }
        this.hpText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "YOU LOSE", textStyle);
        this.hpText.anchor.set(0.5);

        let mainMenuGroup = this.game.add.group();
        let buttonX = this.game.world.centerX;
        let buttonY = this.game.world.centerY;
        let startButton = this.game.make.button(buttonX, buttonY + 100, 'restart', this.actionRestart, this, 2, 1, 0);
        startButton.anchor.set(0.5);
        mainMenuGroup.add(startButton);
    }

    actionRestart() {
        this.game.state.start('GameScene');
    }

}

export default GameOver;