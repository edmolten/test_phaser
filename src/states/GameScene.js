import Spaceship from 'objects/Spaceship';
import Enemy from 'objects/Enemy';

class GameScene extends Phaser.State {

    constructor() {
        super();
        this.player = null;
        this.cursors = null;
        this.fireButton = null;
        this.changeWeaponButton = null;
        this.enemies = null;
        this.maxEnemies = 10;
        this.enemySide = 1;
        this.enemiesKillText = null;
        this.hpText = null;
        this.enemyDestroySound = null;
        this.backgroundMusic = null;
        this.textStyle = {
            font: "12px Arial",
            fill: "#ff0044",
            align: "center"
        }
    }

    preload() {
        this.game.load.image('spaceship', 'assets/spaceship.png');
        this.game.load.image('bullet', 'assets/bullet.png');
        this.game.load.image('bullet2', 'assets/bullet2.png');
        this.game.load.image('enemy', 'assets/enemy.png');
        this.game.load.audio('backgroundMusic', ['assets/audio/thunder_spirits.mp3', 'assets/audio/thunder_spirits.ogg']);
        this.game.load.audio('enemyDestroy', ['assets/audio/laser.mp3', 'assets/audio/laser.ogg']);
        this.game.load.audio('enemyAttack', ['assets/audio/bomb.mp3', 'assets/audio/bomb.ogg']);
    }

    create() {
        this.backgroundMusic = this.game.add.audio('backgroundMusic');
        this.backgroundMusic.loop = true;
        this.backgroundMusic.play();

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.fireButton = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.changeWeaponButton = this.game.input.keyboard.addKey(Phaser.KeyCode.Z);

        let enemyAttackSound = this.game.add.audio('enemyAttack');
        this.player = new Spaceship(this.game, this, this.game.world.centerX, this.game.world.centerY, enemyAttackSound);
        this.player.addWeapon(30, 'bullet');
        //this.player.addWeapon(50, 'bullet2', 100, 20);

        this.enemies = this.game.add.group();
        this.enemies.enableBody = true;

        let hpText = this.game.add.text(10, 10, `HP ${this.player.maxHp}/${this.player.maxHp}`, this.textStyle);
        this.player.setHpText(hpText);
        let enemiesKillText = this.game.add.text(10, 30, `Enemies Killed 0/${this.player.enemiesToWin}`, this.textStyle);
        this.player.setEnemiesKilledText(enemiesKillText);
    }

    createEnemy() {
        if (this.enemies.total < this.maxEnemies) {
            Enemy.createEnemy(this);
        }
    }

    update() {
        this.player.weaponSet.forEach(weapon => this.game.physics.arcade.overlap(weapon.bullets, this.enemies, Enemy.onBulletHit, null, this));
        this.game.physics.arcade.overlap(this.player, this.enemies, Spaceship.onEnemyAttack, null, this);
        this.player.updatePlayer();
        this.enemies.forEach(e => e.updateEnemy());
        this.createEnemy();
    }

}

export default GameScene;